﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;       
    public AudioSource m_ExplosionAudio;              
    public float m_MaxDamage = 100f;                  
    public float m_ExplosionForce = 1000f;            
    public float m_MaxLifeTime = 2f;                  
    public float m_ExplosionRadius = 5f;              


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

            if (!targetRigidbody)
                continue;

            //爆風の力を加えます
            targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);

            //Rigidbodyに関連するTankHealthスクリプトを見つけます
            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();

            // ゲームオブジェクトにアタッチされた TankHealth スクリプトがなければ、次のコライダーをチェックします
            if (!targetHealth)
                continue;

            // 砲弾からの距離に基づいて、ターゲットが受けるダメージ量を計算
            float damage = CalculateDamage(targetRigidbody.position);

            // このダメージをタンクに適用
            targetHealth.TakeDamage(damage);
        }

        // 砲弾とパーティクルの親子関係を解除
        m_ExplosionParticles.transform.parent = null;

        // パーティクルシステムを再生
        m_ExplosionParticles.Play();

        // 爆発のサウンドエフェクトを再生
        m_ExplosionAudio.Play();

        // パーティクルが終了したら、パーティクルを伴っていたゲームオブジェクトを破棄します
        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.duration);

        // 砲弾を破棄
        Destroy(gameObject);
    }



    private float CalculateDamage(Vector3 targetPosition)
    {
        // 砲弾からターゲットまでのベクトルを作成
        Vector3 explosionToTarget = targetPosition - transform.position;
        // 砲弾からターゲットまでの距離を計算
        float explosionDistance = explosionToTarget.magnitude;
        //最大距離 (爆破半径) に対するターゲットの距離の比率を計算
        float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;
        // ダメージの最大値と距離の比率に基づいて、ダメージを計算
        float damage = relativeDistance * m_MaxDamage;
        // ダメージの最小値は常に 0 です。
        damage = Mathf.Max(0f, damage);
        // Calculate the amount of damage a target should take based on it's position.
        return damage;
    }
}