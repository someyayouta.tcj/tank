﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirStrikeSpawn : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [HideInInspector] private float intarval;


    void Start()
    {
        intarval = Random.Range(0.5f, 2f);
        InvokeRepeating("Generate", 1, intarval);
    }

    void Generate()
    {

        Vector3 position = new Vector3(Random.Range(-35.0f, 35.0f), 50, Random.Range(-35.0f, 35.0f));
        Instantiate(prefab, position, Quaternion.identity);
    }


}
