﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairItemManager : MonoBehaviour
{

    [SerializeField] private GameObject m_tank;
    [SerializeField] private float m_RepairValue = 20f;

    private void OnTriggerEnter(Collider col)
    {
        TankHealth tankHealth = col.GetComponent<TankHealth>();

        if (col.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            tankHealth.Repair(m_RepairValue);
        }
    }

}
