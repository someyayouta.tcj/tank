﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairItemSpawn : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [HideInInspector] private float intarval;

    void Start()
    {
        intarval = Random.Range(10f, 15f);
        InvokeRepeating("Generate",7, intarval);
    }

    void Generate()
    {
        Vector3 position = new Vector3(Random.Range(-20.0f, 20.0f), 50, Random.Range(-20.0f, 20.0f));
        Instantiate(prefab, position, Quaternion.identity);
    }
}
